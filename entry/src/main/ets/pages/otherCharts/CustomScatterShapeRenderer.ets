/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { IShapeRenderer } from '@ohos/mpchart';
import { IScatterDataSet } from '@ohos/mpchart';
import { ViewPortHandler } from '@ohos/mpchart';
import {Paint,Style, LinePaint} from '@ohos/mpchart'
import { Utils } from '@ohos/mpchart';

/**
 * Custom shape renderer that draws a single line.
 * Created by philipp on 26/06/16.
 */
export class CustomScatterShapeRenderer implements IShapeRenderer
{


  public renderShape(paints:Paint[], dataSet: IScatterDataSet, viewPortHandler: ViewPortHandler,
                     posX: number, posY: number, renderPaint: Paint): void {

        let shapeHalf: number = dataSet.getScatterShapeSize() / 2;

        renderPaint.setStyle(Style.STROKE);
        renderPaint.setStrokeWidth(Utils.convertDpToPixel(1));

        let linePaint : LinePaint = new LinePaint();
        linePaint.set(renderPaint);
        linePaint.setStyle(Style.STROKE)
        linePaint.setStartPoint([posX - shapeHalf, posY - shapeHalf])
        linePaint.setEndPoint([posX + shapeHalf, posY + shapeHalf])
        paints.push(linePaint);
    }
}
