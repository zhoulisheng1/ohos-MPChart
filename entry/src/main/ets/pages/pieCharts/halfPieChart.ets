/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { PieChart } from '@ohos/mpchart'
import { PieData } from '@ohos/mpchart';
import {PieDataSet,ValuePosition} from '@ohos/mpchart';
import { PieEntry } from '@ohos/mpchart';
import {JArrayList} from '@ohos/mpchart';
import { ColorTemplate } from '@ohos/mpchart'
import { MPPointF } from '@ohos/mpchart';
import {Legend,LegendForm, LegendVerticalAlignment, LegendOrientation, LegendHorizontalAlignment
} from '@ohos/mpchart'
import title from '../title/index';

@Entry
@Component
struct halfPieChart {
  pieData: PieData = null;
  @State pieModel: PieChart.Model = new PieChart.Model()

  parties:string[] = [
    "Party A", "Party B", "Party C", "Party D", "Party E", "Party F", "Party G", "Party H",
    "Party I", "Party J", "Party K", "Party L", "Party M", "Party N", "Party O", "Party P",
    "Party Q", "Party R", "Party S", "Party T", "Party U", "Party V", "Party W", "Party X",
    "Party Y", "Party Z"]
  private menuItemArr: Array<string> = ['View on GitHub']
  private title: string = 'HalfPieChartActivity'
  @State @Watch("menuCallback") model: title.Model = new title.Model()
  aboutToAppear():void {
    this.pieData = this.initPieData(4, 10);
    this.pieModel
      .setPieData(this.pieData)
      .setRadius(150)
      .setHoleRadius(0.5)
      .setMaxAngle(180)
      .setOffset(new MPPointF(160,200))   // vp
      .setRotationAngle(180)
      .setUsePercentValues(true)
      .setRotationEnabled(false)

    let l:Legend = this.pieModel.getLegend()
    l.setOrientation(LegendOrientation.HORIZONTAL)
    l.setVerticalAlignment(LegendVerticalAlignment.TOP);
//    l.setHorizontalAlignment(LegendHorizontalAlignment.CENTER);
    l.setYEntrySpace(5)
    l.setFormToTextSpace(5)

    this.model.menuItemArr = this.menuItemArr
    this.model.title = this.title
  }

// 初始化饼状图数据
  private initPieData(count:number, range:number):PieData{
    let entries = new JArrayList<PieEntry>();
    for (var i = 0; i < count ; i++) {
      entries.add(new PieEntry(((Math.random() * range) + range / 5),this.parties[i % this.parties.length]))
    }
    let dataSet:PieDataSet = new PieDataSet(entries, "Election Results");
    dataSet.setDrawIcons(false);
    dataSet.setSliceSpace(3);
    dataSet.setIconsOffset(new MPPointF(0, 40));
    dataSet.setSelectionShift(5);
    dataSet.setColorsByVariable(ColorTemplate.MATERIAL_COLORS);
//    dataSet.setYValuePosition(ValuePosition.OUTSIDE_SLICE);
    return new PieData(dataSet)
  }

  menuCallback() {
    if (this.model == null || this.model == undefined) {
      return
    }
    let index: number = this.model.getIndex()

    switch (index) {
      case 0:
      //TODO View on GitHub
        break;
    }
    this.model.setIndex(-1)
  }

  build() {
    Column() {
//      title({ model: this.model })
      PieChart({
        model: $pieModel
      })
    }
  }
}