/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import {ScatterShape} from '@ohos/mpchart'
import { ScatterData } from '@ohos/mpchart';
import {ScatterDataSet} from '@ohos/mpchart';
import { IScatterDataSet } from '@ohos/mpchart';
import { MyRect } from '@ohos/mpchart';
import { LegendEntry } from '@ohos/mpchart';
import { TextPaint } from '@ohos/mpchart';
import {XAxis, XAxisPosition} from '@ohos/mpchart';
import { YAxis, AxisDependency, YAxisLabelPosition} from '@ohos/mpchart'
import { EntryOhos } from '@ohos/mpchart';
import {JArrayList} from '@ohos/mpchart';
import { YAxisView } from '@ohos/mpchart';
import { XAxisView } from '@ohos/mpchart';
import { Utils,MPPointF } from '@ohos/mpchart';
import { MultipleLegend } from '@ohos/mpchart';
import { ScatterView } from '@ohos/mpchart'
import {ScatterShapeAllType} from '@ohos/mpchart'
import { ScaterChartMode } from '@ohos/mpchart';
import { ScatterChart }  from '@ohos/mpchart';
import {LimitLine} from '@ohos/mpchart';

@Entry
@Component
export default struct fragSimpleScatterIndex {
//@State
  topAxis: XAxis = new XAxis(); //顶部X轴
//@State
  bottomAxis: XAxis = new XAxis(); //底部X轴
//@State
  mWidth: number = 350; //表的宽度
//@State
  mHeight: number = 600; //表的高度
//@State
  minOffset: number = 15; //X轴线偏移量
//@State
  leftAxis: YAxis = new YAxis();
//@State
  rightAxis: YAxis = new YAxis();
  XLimtLine:LimitLine= new LimitLine(35, "Index 10");

  legendScatter:LegendEntry=new LegendEntry();

// @State
  legendColorWidth: number = 10;
// @State
  legendColorHeight: number = 10;
  legendArr:LegendEntry[][]=[[],[],[]]
  dataScatter: ScatterData = null;
  @State
  scaterChartMode:ScaterChartMode=new ScaterChartMode();
  build() {
    Column() {
      Column() {
        Stack({ alignContent: Alignment.TopStart }) {
          ScatterChart({
            scaterChartMode: this.scaterChartMode
          })
        }
      }
//      Column() {
//        //        ForEach(this.legendArr.map((data, index) => {
//        //          return { index: index, legendItem: data };
//        //        }), (item) => {
//        //          Row() {
//        //            ForEach(item.legendItem.map((dataChild, indexChild) => {
//        //              return { indexChild: indexChild, legendItem: dataChild };
//        //            }), (itemChild) => {
//        //                MultipleLegend({ legend: itemChild.legendItem });
//        //                Blank().width(4)
//        //            }, itemChild => itemChild.indexChild)
//        //          }.alignItems(VerticalAlign.Top)
//        //                Blank().height(4)
//        //        }, item => item.index)
//        ForEach(this.legendArr, (item:LegendEntry[]) => {
//          Row() {
//            ForEach(item, (itemChild: LegendEntry) => {
//              MultipleLegend({ legend: itemChild });
//              Blank().width(4)
//            }, (itemChild: LegendEntry) => (itemChild.label + "").toString())
//          }.alignItems(VerticalAlign.Top)
//          Blank().height(4)
//
//        },(item: LegendEntry[]) => (item[0].label + "").toString())
//      }.margin({left:50})
    }
    .alignItems(HorizontalAlign.Start)
  }

  aboutToAppear() {

    this.dataScatter=this.generateScatterData(6, 10000, 20);
    let textPaint:TextPaint = new TextPaint();
    textPaint.setTextSize(this.leftAxis.getTextSize());

    var leftTextWidth = Utils.calcTextWidth(textPaint,this.getFormattedValue(this.dataScatter.getYMax()));
    var rightTextWidth = Utils.calcTextWidth(textPaint,this.getFormattedValue(this.dataScatter.getYMax()));
    let left = this.minOffset + leftTextWidth;
    let top = this.minOffset;
    let right = this.mWidth - this.minOffset - rightTextWidth;
    let bottom = this.mHeight - this.minOffset;
    let myRect:MyRect = new MyRect(left, top, right, bottom);
    this.dataScatter.mDisplayRect = myRect;

    this.topAxis.setLabelCount(7, false);
    this.topAxis.setPosition(XAxisPosition.TOP);
    this.topAxis.setAxisMinimum(0);
    this.topAxis.setAxisMaximum(this.dataScatter.getXMax());
    this.topAxis.enableGridDashedLine(10,10,0)
    this.topAxis.setDrawAxisLine(false);
    this.topAxis.setDrawLabels(false)
    this.topAxis.setDrawGridLines(true);


    this.bottomAxis.setLabelCount(7, false);
    this.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.bottomAxis.setAxisMinimum(0);
    this.bottomAxis.setAxisMaximum(this.dataScatter.getXMax());
    this.bottomAxis.setDrawAxisLine(true);
    this.bottomAxis.setDrawLabels(true)


    this.leftAxis = new YAxis(AxisDependency.LEFT);
    this.leftAxis.setLabelCount(6, false);
    this.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.leftAxis.setSpaceTop(15);
    this.leftAxis.setAxisMinimum(this.dataScatter.getYMin()-500);
    this.leftAxis.setAxisMaximum(this.dataScatter.getYMax()+1000);
    this.leftAxis.setDrawGridLines(true);
    this.leftAxis.enableGridDashedLine(10,10,0)
    this.leftAxis.setDrawLabels(true)

    this.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.rightAxis.setDrawGridLines(false);
    this.rightAxis.setLabelCount(6, false);
    this.rightAxis.setSpaceTop(15);
    this.rightAxis.setAxisMinimum(this.dataScatter.getYMin()-500); // this replaces setStartAtZero(true)
    this.rightAxis.setAxisMaximum(this.dataScatter.getYMax()+1000);
    this.rightAxis.setDrawAxisLine(true);
    this.rightAxis.setDrawLabels(true)
    this.rightAxis.setDrawLabels(true)

    //this.scaterChartMode.setYExtraOffset(this.model.height)
//    this.scaterChartMode.topAxis=this.topAxis
//    this.scaterChartMode.bottomAxis=this.bottomAxis
//    this.scaterChartMode.width=this.width
//    this.scaterChartMode.height=this.height
//    this.scaterChartMode.minOffset=this.minOffset
//    this.scaterChartMode.leftAxis=this.leftAxis
//    this.scaterChartMode.rightAxis=this.rightAxis
//    this.scaterChartMode.data=this.dataScatter
//    this.scaterChartMode.init()

    //this.scaterChartMode.setYExtraOffset(this.model.height)
    this.scaterChartMode.topAxis=this.topAxis
    this.scaterChartMode.bottomAxis=this.bottomAxis
    this.scaterChartMode.mWidth=this.mWidth
    this.scaterChartMode.mHeight=this.mHeight
    this.scaterChartMode.minOffset=this.minOffset
    this.scaterChartMode.leftAxis=this.leftAxis
    this.scaterChartMode.rightAxis=this.rightAxis
    this.scaterChartMode.data=this.dataScatter
    this.scaterChartMode.XLimtLine=this.XLimtLine
    this.scaterChartMode.init()
    this.setLegend();
  }

/**
   * 初始化数据
   * @param count  曲线图点的个数
   * @param range  y轴范围
   */
  private  generateScatterData(dataSets:number, range:number,count:number):ScatterData {
    let sets: JArrayList<IScatterDataSet> = new JArrayList<IScatterDataSet>();
    let shapes:ScatterShape[]  = ScatterShapeAllType
    for(let i = 0; i < dataSets; i++) {

      let entries:JArrayList<EntryOhos> =  new JArrayList<EntryOhos>()

      for(let j = 0; j < count; j++) {
        entries.add(new EntryOhos(j, Math.random() * range + range / 5));
      }
      let ds:ScatterDataSet= new ScatterDataSet(entries, this.mLabels[i]);
      ds.setScatterShape(shapes[i % shapes.length]);
      ds.setColorsByArr([0xc12552,0xff6600,0xf5c700,0x6a961f,0xb36435]);
      ds.setScatterShapeSize(px2vp(9));
      ds.setDrawValues(false);
      ds.setIconsOffset(new MPPointF(0, px2vp(0)));
      //ds.setValueTextSize(10);
      sets.add(ds);
    }
    return new ScatterData(sets);
  }
  public setLegend(){

    for(let i=0; i<this.dataScatter.getDataSets().size(); i++) {
      let dataSet = this.dataScatter.getDataSetByIndex(i)
      let legendLine:LegendEntry =new LegendEntry();
      legendLine.colorHeight=10
      legendLine.colorHeight=10
      legendLine.colorItemSpace=3
      legendLine.colorLabelSpace=4
      legendLine.labelColor=Color.Black
      legendLine.labelTextSize=10
      legendLine.colorArr=dataSet.getColors().dataSouce
      legendLine.label=dataSet.getLabel()
      this.legendArr[Math.floor(i/2)][i%2]= legendLine;
    }
  }
  private getFormattedValue(value: number): string {
    return value.toFixed(0)
  }
  private  mLabels:string[] = [ "Company A", "Company B", "Company C", "Company D", "Company E", "Company F" ];


}