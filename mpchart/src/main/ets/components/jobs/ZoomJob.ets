/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import BarLineChartBase from '../charts/BarLineChartBase';
import YAxis,{AxisDependency}from '../components/YAxis';
import {ObjectPool} from '../utils/ObjectPool';
import {Poolable} from '../utils/Poolable';
import Transformer from '../utils/Transformer';
import ViewPortHandler from '../utils/ViewPortHandler';
import ViewPortJob from '../jobs/ViewPortJob';
import ComponentBase from '../components/ComponentBase';
import Matrix from '../utils/Matrix';
import Chart from '../charts/Chart'
export default class ZoomJob extends ViewPortJob {

    private static pool:ObjectPool<ZoomJob> =  ObjectPool.create(1, new ZoomJob(null, 0, 0, 0, 0, null, null, null)).setReplenishPercentage(0.5);

    public static  getInstance(viewPortHandler:ViewPortHandler,scaleX:number,scaleY:number, xValue:number, yValue:number,
                               trans:Transformer,axis:AxisDependency, v:Chart<any>):ZoomJob{
        let result:ZoomJob= ZoomJob.pool.get();
        result.xValue = xValue;
        result.yValue = yValue;
        result.scaleX = scaleX;
        result.scaleY = scaleY;
        result.mViewPortHandler = viewPortHandler;
        result.mTrans = trans;
        result.axisDependency = axis;
        result.view = v;
        return result;
    }

    public static recycleInstance(instance:ZoomJob):void {
        ZoomJob.pool.recycle(instance);
    }

    protected scaleX:number;
    protected scaleY:number;

    protected axisDependency:AxisDependency;

    constructor(viewPortHandler:ViewPortHandler,scaleX:number,scaleY:number, xValue:number, yValue:number,trans:Transformer,
                    axis:AxisDependency, v:ComponentBase) {
        super(viewPortHandler, xValue, yValue, trans, v);

        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.axisDependency = axis;
    }

    protected  mRunMatrixBuffer:Matrix = new Matrix();

    public run():void {

        let save:Matrix = this.mRunMatrixBuffer;
        this.mViewPortHandler.zoom(this.scaleX,this.scaleY,null,null,save);
        this.mViewPortHandler.refresh(save, this.view, false);

        let yValsInView:number = (this.view as BarLineChartBase).getAxis(this.axisDependency).mAxisRange / this.mViewPortHandler.getScaleY();
        let xValsInView:number = (this.view as BarLineChartBase).getXAxis().mAxisRange / this.mViewPortHandler.getScaleX();

        this.pts[0] = this.xValue - xValsInView / 2;
        this.pts[1] = this. yValue + yValsInView / 2;

        this.mTrans.pointValuesToPixel(this.pts);

        this.mViewPortHandler.translate(this.pts, save);
        this.mViewPortHandler.refresh(save,  this.view, false);

        (this.view as BarLineChartBase).calculateOffsets();
        this.view.postInvalidate();

        ZoomJob.recycleInstance(this);
    }

    public  instantiate():Poolable {
        return new ZoomJob(null, 0, 0, 0, 0, null, null, null);
    }
}
