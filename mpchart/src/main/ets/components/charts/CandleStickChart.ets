/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import MyCandleData from '../data/MyCandleData';
import { XAxis, XAxisPosition } from '../components/XAxis';
import YAxis, { AxisDependency, YAxisLabelPosition } from '../components/YAxis'
import XAxisView from '../components/renderer/XAxisView';
import YAxisView, { YAxisModel } from '../components/renderer/YAxisView'
import MPPointF from '../utils/MPPointF'
import MyRect from '../data/Rect';
import Utils from '../utils/Utils'
import ViewPortHandler from '../utils/ViewPortHandler';
import ChartAnimator from '../animation/ChartAnimator'
import MyCandleRenderer from '../renderer/MyCandleRenderer'
import Paint, { LinePaint, RectPaint, TextPaint, Style } from '../data/Paint'
import MyCandleEntry from '../data/MyCandleEntry';
import { JArrayList } from '../utils/JArrayList';
import MyCandleDataSet from '../data/MyCandleDataSet'
import ScaleMode from '../data/ScaleMode'


@Component
struct CandleStickChart {
  @State model: CandleStickChart.Model=new CandleStickChart.Model()
  @State @Watch("Refresh") mCount: number = 40;
  @State @Watch("Refresh") mRanged: number = 100;
  visibilityAxis: Visibility = Visibility.Visible;

  build() {
    Column() {
      Stack({ alignContent: Alignment.TopStart }) {
        Stack() {
          XAxisView({
            scaleMode: this.model
          });
        }.visibility(this.visibilityAxis)
        .clip(new Path().commands(this.model.calcClipPath(2))).zIndex(1)

        Stack() {
          YAxisView({
            model: this.model.leftAxisModel
          })
          YAxisView({ model: this.model.rightAxisModel })
        }.visibility(this.visibilityAxis)
        .clip(new Path().commands(this.model.calcClipPath(3))).zIndex(1)

        Stack({ alignContent: Alignment.TopStart }) {
          if (this.model.mPaints != null && this.model.mPaints != undefined && this.model.mPaints.length > 0) {
            ForEach(this.model.mPaints, (item: Paint) => {
              if (item instanceof LinePaint) {
                Line()
                  .width(this.model.mWidth)
                  .height(this.model.mHeight)
                  .startPoint(item.startPoint)
                  .endPoint(item.endPoint)
                  .fill(item.fill)
                  .stroke(item.stroke)
                  .strokeWidth(1)
                  .position({ x: 0, y: 0 })
                  .visibility(item.visibility)
              } else if (item instanceof RectPaint) {
                if (item.getStyle() == Style.FILL) {
                  Rect({ width: item.width, height: item.height })
                    .position({ x: item.x, y: item.y })
                    .fill(item.fill)
                    .visibility(item.visibility)
                } else {
                  Rect({ width: item.width, height: item.height })
                    .position({ x: item.x, y: item.y })
                    .fill('none')
                    .borderWidth(2)
                    .borderColor(Color.Green)
                    .visibility(item.visibility)
                }
              }
              else if (item instanceof TextPaint) {
                Text(item.text)
                  .fontSize(item.textSize)
                  .textAlign(item.textAlign)
                  .position({ x: 0, y: 0 })
                  .zIndex(999)
              }
            }, item => JSON.stringify(item))
          }
          Line()
            .width(this.model.mWidth)
            .height(this.model.mHeight)
            .startPoint(this.model.xStartPoint)
            .endPoint(this.model.xEndPoint)
            .fill(Color.Red)
            .stroke(Color.Red)
            .strokeWidth(0.5)
            .position({ x: 0, y: 0 })
          Line()
            .width(this.model.mWidth)
            .height(this.model.mHeight)
            .startPoint(this.model.yStartPoint)
            .endPoint(this.model.yEndPoint)
            .fill(Color.Red)
            .stroke(Color.Red)
            .strokeWidth(0.5)
            .position({ x: 0, y: 0 })
        }
        .clip(new Path().commands(this.model.calcClipPath(1))).zIndex(999)
        .size({ width: '100%', height: '100%' })
      }
      .onClick((event: ClickEvent) => {
        this.model.onClick(event)
      }).onTouch((event: TouchEvent) => {
        this.model.onTouch(event)
      })
      .size({ width: '100%', height: '85%' })

      /* Row({}) {
         Slider({
           value: this.mCount,
           min: 40,
           max: 200,
           step: 1,
           style: SliderStyle.OutSet
         })
           .blockColor(Color.Green)
           .trackColor(Color.Gray)
           .selectedColor(Color.Blue)
           .showSteps(false)
           .showTips(false)
           .onChange((value: number, mode: SliderChangeMode) => {
             this.mCount = value
           })
         Text(this.mCount.toFixed(0)).fontSize(16)
       }
       .width('80%')

       Row() {
         Slider({
           value: this.mRanged,
           min: 0,
           max: 200,
           step: 1,
           style: SliderStyle.OutSet
         })
           .blockColor(Color.Green)
           .trackColor(Color.Gray)
           .selectedColor(Color.Blue)
           .showSteps(false)
           .showTips(false)
           .onChange((value: number, mode: SliderChangeMode) => {
             this.mRanged = value
           })
         Text(this.mRanged.toFixed(0)).fontSize(16)
       }
       .width('80%')*/
    }
  }

  public aboutToAppear() {
    this.model.rectInit()
    this.model.topAxis.setLabelCount(5, false);
    this.model.topAxis.setPosition(XAxisPosition.TOP);
    this.model.topAxis.setAxisMinimum(0);
    this.model.topAxis.setAxisMaximum(this.mCount);

    this.model.bottomAxis.setLabelCount(5, false);
    this.model.bottomAxis.setPosition(XAxisPosition.BOTTOM);
    this.model.bottomAxis.setAxisMinimum(0);
    this.model.bottomAxis.setAxisMaximum(this.mCount);

    this.model.leftAxis = new YAxis(AxisDependency.LEFT);
    this.model.leftAxis.setLabelCount(8, false);
    this.model.leftAxis.setPosition(YAxisLabelPosition.OUTSIDE_CHART);
    this.model.leftAxis.setSpaceTop(8);
    this.model.leftAxis.setAxisMinimum(80);
    this.model.leftAxis.setAxisMaximum(150);
    this.model.leftAxis.enableGridDashedLine(10, 10, 0)


    this.model.rightAxis = new YAxis(AxisDependency.RIGHT);
    this.model.rightAxis.setDrawGridLines(false);
    this.model.rightAxis.setLabelCount(8, false);
    this.model.rightAxis.setSpaceTop(8);
    this.model.rightAxis.setAxisMinimum(80); // this replaces setStartAtZero(true)
    this.model.rightAxis.setAxisMaximum(150);
    this.model.rightAxis.setDrawAxisLine(false);
    this.model.rightAxis.setDrawLabels(false)

    this.model.init()

    this.model.leftAxisModel.setYAxis(this.model.leftAxis)
    this.model.leftAxisModel.setHeight(this.model.mHeight)
    this.model.leftAxisModel.setWidth(this.model.mWidth)
    this.model.leftAxisModel.setMinOffset(this.model.minOffset)

    this.model.rightAxisModel.setWidth(this.model.mWidth)
    this.model.rightAxisModel.setHeight(this.model.mHeight)
    this.model.rightAxisModel.setMinOffset(this.model.minOffset)
    this.model.rightAxisModel.setYAxis(this.model.rightAxis)

    this.model.xAixsMode.topAxis = this.model.topAxis
    this.model.xAixsMode.bottomAxis = this.model.bottomAxis
    this.model.xAixsMode.mWidth = this.model.mWidth
    this.model.xAixsMode.mHeight = this.model.mHeight
    this.model.xAixsMode.minOffset = this.model.minOffset
    this.model.xAixsMode.yLeftLongestLabel = this.model.leftAxis.getAxisMaximum() + ""
    this.model.xAixsMode.yRightLongestLabel = this.model.leftAxis.getAxisMaximum() + ""
  }

  private Refresh(): void {
    let values = new JArrayList<MyCandleEntry>();
    let x: number = 20;
    for (let i: number = 0; i < this.mCount && x < this.model.mWidth; i++) {
      let val: number = Math.random() * 400 + 100; // 100 ~ 500
      let high: number = Math.random() * 40 + 60; // 60 ~ 100
      let low: number = Math.random() * 40 + 60;
      let open: number = Math.random() * 10 + 40; // 40 ~ 50
      let close: number = Math.random() * 10 + 40;
      let even: boolean = i % 2 == 0;

      x += 20;
      values.add(new MyCandleEntry(x,
        val + high,
        val - low,
          even ? val + open : val - open,
          even ? val - close : val + close));
    }

    let dataSets = new JArrayList<MyCandleDataSet>();

    let set1 = new MyCandleDataSet(values, "Data Set");
    dataSets.add(set1);

    this.model.mRender = new MyCandleRenderer(new MyCandleData(dataSets), this.model.mAnimator, this.model.mViewPortHandler, this.model);
    this.model.mPaints = []
    this.model.mPaints = this.model.mPaints.concat(this.model.mRender.drawData());
  }
}

namespace CandleStickChart {
  @Observed
  export class Model extends ScaleMode {
    candleData: MyCandleData = new MyCandleData();
    topAxis: XAxis = new XAxis();
    bottomAxis: XAxis = new XAxis();
    leftAxis: YAxis = new YAxis();
    rightAxis: YAxis = new YAxis();
    mWidth: number = 380; //表的宽度
    mHeight: number = 600; //表的高度
    minOffset: number = 15
    scaleX: number = 1
    scaleY: number = 1
    xStartPoint: number[] = [];
    xEndPoint: number[] = [];
    yStartPoint: number[] = [];
    yEndPoint: number[] = [];
    mPaints: Paint[] = []
    mAnimator: ChartAnimator = new ChartAnimator();
    mViewPortHandler: ViewPortHandler = new ViewPortHandler();
    mRender: MyCandleRenderer = null;
    mData: MyCandleData = new MyCandleData();
    curvesX: number = 0;
    curvesY: number = 0;
    timerIdX: number = -1;
    timerIdY: number = -1;
    clipPath: string = ''

    setCandleData(candleData: MyCandleData): Model{
      this.candleData = candleData
      return this
    }

    computeHeightlight(x: number, y: number) {
      let ccTemp: number = Number.MAX_VALUE;
      let positionX: number;
      let positionY: number;
      let rect = this.candleData.mDisplayRect;
      let size = this.candleData.getDataSetCount();
      if (x > rect.left && x < rect.right && y > rect.top && y < rect.bottom) {
        //      let point: MPPointF = this.calculateCoordinate(x, y)
        for (let i = 0;i < size; i++) {
          let entryArray: MyCandleEntry[] = (this.candleData.getDataSetByIndex(i) as MyCandleDataSet).getEntries()
            .toArray();
          for (let j = 0; j < entryArray.length; j++) {
            let entryX: number;
            let entryY: number;
            entryX = entryArray[j].getX()
            entryY = entryArray[j].getY()
            let a = Math.abs(x - entryX);
            let cc = Math.sqrt(a * a);
            if (ccTemp > cc) {
              ccTemp = cc;
              positionX = entryX;
              positionY = entryY;
            }
          }
        }
      }

      this.xStartPoint = [rect.left, (positionY * this.scaleX - 2 + this.moveY - this.currentYSpace)];
      this.xEndPoint = [rect.right, (positionY * this.scaleX - 2 + this.moveY - this.currentYSpace)];

      this.yStartPoint = [(positionX * this.scaleX - 2 + this.moveX - this.currentXSpace), rect.top];
      this.yEndPoint = [(positionX * this.scaleX - 2 + this.moveX - this.currentXSpace), rect.bottom];
    }

    rectInit() {
      var leftTextWidth = Utils.calcTextWidth(new TextPaint(), "AAAA");
      let left = this.leftAxis.getSpaceTop() + leftTextWidth;
      let top = this.minOffset;
      let right = this.mWidth - left;
      let bottom = this.mHeight - this.minOffset;
      this.candleData.mDisplayRect = new MyRect(left, top, right, bottom);
    }

    // 计算落点的坐标
    calculateCoordinate(touchX: number, touchY: number): MPPointF{
      let rect = this.candleData.mDisplayRect;
      this.scaleX = (rect.right - rect.left) / (this.topAxis.mAxisMaximum - this.topAxis.mAxisMinimum);
      this.scaleY = (rect.bottom - rect.top) / (this.leftAxis.mAxisMaximum - this.leftAxis.mAxisMinimum);
      let touchXVp = this.topAxis.mAxisMinimum + (touchX - rect.left) / this.scaleX
      // px换算成vp
      let touchYVp = this.leftAxis.mAxisMaximum - ((touchY - rect.top) / this.scaleY)
      let point = new MPPointF()
      point.x = touchXVp
      point.y = touchYVp
      return point
    }

    getCandleData(): MyCandleData{
      return this.candleData
    }

    init() {
      let paintsTemp: Paint[] = [];
      this.mViewPortHandler.setChartDimens(this.mWidth, this.mHeight);
      this.mRender = new MyCandleRenderer(this.candleData, this.mAnimator, this.mViewPortHandler, this);
      paintsTemp = paintsTemp.concat(this.mRender.drawData());
      this.mPaints = []
      this.mPaints = this.mPaints.concat(paintsTemp)
    }

    public animateX(durationMillis: number) {
      let count = this.mPaints.length;
      this.curvesX = 0;
      clearInterval(this.timerIdX)
      this.timerIdX = setInterval(() => {
        this.curvesX += 10;
        let position = Math.floor(this.curvesX / (durationMillis / count))
        for (let i = 0;i < count; i++) {
          this.mPaints[i].setVisibility(position >= i ? Visibility.Visible : Visibility.Hidden)
        }
        if (this.curvesX >= durationMillis) {
          this.curvesX = 0;
          clearInterval(this.timerIdX)
        }
      }, 10)
    }

    public animateY(durationMillis: number) {
      this.curvesY = 0;
      clearInterval(this.timerIdY)
      this.timerIdY = setInterval(() => {
        this.curvesY += 0.01 / (durationMillis / 1000)
        this.mAnimator.setPhaseY(this.curvesY)
        this.init()
        if (this.curvesY >= 1) {
          this.curvesY = 0;
          clearInterval(this.timerIdY)
        }
      }, 30)
    }

    public animateXY(durationMillis: number) {
      this.animateX(durationMillis)
      this.animateY(durationMillis)
    }

    public calcClipPath(num:number):string{
      let rect = this.candleData.mDisplayRect;
      let path = ''
      if(num == 1){
        path = 'M'+Utils.convertDpToPixel(rect.left)+' '+ Utils.convertDpToPixel(rect.top)
        +'L'+Utils.convertDpToPixel(rect.right)+' '+ Utils.convertDpToPixel(rect.top)
        +'L'+Utils.convertDpToPixel(rect.right)+' '+Utils.convertDpToPixel(rect.bottom - this.minOffset)
        +'L'+Utils.convertDpToPixel(rect.left)+' '+Utils.convertDpToPixel(rect.bottom - this.minOffset)
        +' Z'

      } else if(num == 2){
        path = 'M' + Utils.convertDpToPixel(rect.left - 2) + ' ' + Utils.convertDpToPixel(0)
        + 'L' + Utils.convertDpToPixel(rect.right + 8) + ' ' + Utils.convertDpToPixel(0)
        + 'L' + Utils.convertDpToPixel(rect.right + 8) + ' ' + Utils.convertDpToPixel(this.mHeight)
        + 'L' + Utils.convertDpToPixel(rect.left - 2) + ' ' + Utils.convertDpToPixel(this.mHeight)
        + ' Z'
      } else {
        // y
        path = 'M' + Utils.convertDpToPixel(this.minOffset/2) + ' ' + Utils.convertDpToPixel(rect.top - 3)
        + 'L' + Utils.convertDpToPixel(rect.right + 8) + ' ' + Utils.convertDpToPixel(rect.top - 3)
        + 'L' + Utils.convertDpToPixel(rect.right + 8) + ' ' + Utils.convertDpToPixel(this.mHeight)
        + 'L' + Utils.convertDpToPixel(this.minOffset/2) + ' ' + Utils.convertDpToPixel(this.mHeight)
        + ' Z'
      }
      return path
    }

    public onDoubleClick(event: ClickEvent) {
      this.scaleX = Number(this.scaleX.toFixed(1))
      this.scaleY = Number(this.scaleY.toFixed(1))
      console.log('this.scaleX:' + this.scaleX)
      this.currentXSpace = this.centerX * this.scaleX - this.centerX
      this.currentYSpace = this.centerY * this.scaleY - this.centerY
      let moveYSource=this.leftAxisModel.lastHeight*this.scaleY - this.leftAxisModel.lastHeight
      this.leftAxisModel.translate(moveYSource+this.moveY-this.currentYSpace);
      this.setXPosition(this.moveX - this.currentXSpace)
      this.init()
      if (event && event.screenX) {
        this.computeHeightlight(event.screenX, event.screenY)
      }
    }

    public onMove(event: TouchEvent) {
      let finalMoveX = this.currentXSpace - this.moveX
      let finalMoveY = this.currentYSpace - this.moveY
      if (this.moveX > 0 && finalMoveX <= 0) {
        this.moveX = this.currentXSpace
      }
      if (this.moveY > 0 && finalMoveY <= 0) {
        this.moveY = this.currentYSpace
      }
      if (this.moveX - this.currentXSpace <= this.mWidth - this.xAixsMode.mWidth) {
        this.moveX = this.mWidth - this.xAixsMode.mWidth + this.currentXSpace
      }
      let scaleYHeight = this.mHeight * this.scaleY
      if (this.moveY - this.currentYSpace <= this.mHeight - scaleYHeight) {
        this.moveY = this.mHeight - scaleYHeight + this.currentYSpace
      }
      this.init()
      if (event && event.touches && event.touches[0] && event.touches[0].screenX) {
        this.computeHeightlight(event.touches[0].screenX, event.touches[0].screenY);
      }
      let moveYSource=this.leftAxisModel.lastHeight*this.scaleY - this.leftAxisModel.lastHeight
      this.leftAxisModel.translate(moveYSource+this.moveY-this.currentYSpace);
      this.setXPosition(this.moveX - this.currentXSpace)
    }
    /**
     * 需要覆写的平移事件 的缩放事件
     * @param event
     */
    public onScale(event: TouchEvent) {
      this.onDoubleClick(null)
//      this.scaleX = Number(this.scaleX.toFixed(1))
//      this.scaleY = Number(this.scaleY.toFixed(1))
//      console.log('this.scaleX:' + this.scaleX)
//      this.currentXSpace = this.centerX * this.scaleX - this.centerX
//      this.currentYSpace = this.centerY * this.scaleY - this.centerY
//      this.computeHeightlight(event.touches[0].screenX, event.touches[0].screenY);
//      this.init()
    }

    public onSingleClick(event: ClickEvent) {
      let x = event.screenX
      let y = event.screenY
      this.computeHeightlight(x, y);
    }
  }
}

export default CandleStickChart


