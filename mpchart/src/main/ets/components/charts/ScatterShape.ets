/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Predefined ScatterShapes that allow the specification of a shape a ScatterDataSet should be drawn with.
 * If a ScatterShape is specified for a ScatterDataSet, the required renderer is set.
 */
export enum ScatterShape {
  SQUARE,
  CIRCLE,
  TRIANGLE,
  CROSS,
  X,
  CHEVRON_UP,
  CHEVRON_DOWN,

}
export const ScatterShapeAllType:ScatterShape[]=[ScatterShape.SQUARE, ScatterShape.CIRCLE, ScatterShape.TRIANGLE, ScatterShape.CROSS, ScatterShape.X, ScatterShape.CHEVRON_UP, ScatterShape.CHEVRON_DOWN]