/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import IShapeRenderer from './IShapeRenderer';
import IScatterDataSet from '../../interfaces/datasets/IScatterDataSet';
import ViewPortHandler from '../../utils/ViewPortHandler';
import Utils from '../../utils/Utils';
import Paint,{Style, LinePaint} from '../../data/Paint'

/**
 * Created by wajdic on 15/06/2016.
 * Created at Time 09:08
 */
export default class ChevronDownShapeRenderer implements IShapeRenderer
{


  public renderShape(paints:Paint[], dataSet: IScatterDataSet, viewPortHandler: ViewPortHandler,
                                    posX: number, posY: number, renderPaint: Paint): void {

        let shapeHalf: number = dataSet.getScatterShapeSize() / 2;

        renderPaint.setStyle(Style.STROKE);
        renderPaint.setStrokeWidth(Utils.convertDpToPixel(1));

        let linePaint1 : LinePaint = new LinePaint();
        linePaint1.set(renderPaint);
        linePaint1.setStyle(Style.STROKE)
        linePaint1.setStartPoint([posX, posY+ (2 * shapeHalf)])
        linePaint1.setEndPoint([posX + (2 * shapeHalf), posY])
        paints.push(linePaint1);

        let linePaint2 : LinePaint = new LinePaint();
        linePaint2.set(renderPaint);
        linePaint2.setStyle(Style.STROKE)
        linePaint2.setStartPoint([posX, posY+ (2 * shapeHalf)])
        linePaint2.setEndPoint([posX - (2 * shapeHalf), posY])
        paints.push(linePaint2);
    }
}
