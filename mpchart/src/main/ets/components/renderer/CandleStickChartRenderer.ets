/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// @ts-nocheck
import Paint,{LinePaint,CirclePaint,RectPaint,TextPaint} from '../data/paint';
import { ImagePaint } from './Paint';

import ChartAnimator from '../animation/ChartAnimator'
import CandelData from '../data/CandleData';
import CandleEntry from '../data/CandleEntry';
import {CandleDataProvider} from '../interfaces/dataprovider/CandleDataProvider';
import ICandleDataSet from '../interfaces/datasets/ICandleDataSet';
import Highlight from '../Highlight/Highlight';
import {Color,ColorTemplate} from '../utils/ColorTemplate';
import MPPointD from '../utils/MPPointD';
import MPPointF from '../utils/MPPointF';
import Transformer from '../utils/Transformer'
import Utils from '../utils/Utils'
import ViewPortHandler from '../utils/viewporthandler'

import {JArrayList} from '../utils/JArrayList';
import LineScatterCandleRadarRenderer from '../renderer/LineScatterCandleRadarRenderer'

export default class CandleStickChartRenderer extends LineScatterCandleRadarRenderer {

    protected mChart: CandleDataProvider;

    private mShadowBuffers: number[] = [];
    private mBodyBuffers: number[] = [];
    private mRangeBuffers: number[] = [];
    private mOpenBuffers: number[] = [];
    private mCloseBuffers: number[] = [];

    constructor(chart: CandleDataProvider, animator: ChartAnimator,
                                    viewPortHandler: ViewPortHandler) {
        super(animator, viewPortHandler);
        this.mChart = chart;
    }

    public drawData(): Paint[] {

        let paints: Paint[] = [];
        let candleData: CandleData = this.mChart.getCandleData();

        for (let data in candleData.getDataSets()) {

            if (data.isVisible())
                paints.concat(this.drawDataSet(data));
        }
        return paints;
    }

    protected drawDataSet(dataSet: ICandleDataSet): paint[] {

        let paints: Paint[] = [];
        let trans: Transformer = this.mChart.getTransformer(dataSet.getAxisDependency());

        let phaseY: number = mAnimator.getPhaseY();
        let barSpace: number = dataSet.getBarSpace();
        let showCandleBar: boolean = dataSet.getShowCandleBar();

        this.mXBounds.set(mChart, dataSet);

        this.mRenderPaint.setStrokeWidth(dataSet.getShadowWidth());

        // draw the body
        for (let j: number = this.mXBounds.min; j <= this.mXBounds.range + this.mXBounds.min; j++) {

            // get the entry
            let e: CandleEntry = dataSet.getEntryForIndex(j);

            if (e == null)
                continue;

            let xPos: number = e.getX();

            let open: number = e.getOpen();
            let close: number = e.getClose();
            let high: number = e.getHigh();
            let low: number = e.getLow();

            if (showCandleBar) {
                // calculate the shadow

                this.mShadowBuffers[0] = xPos;
                this.mShadowBuffers[2] = xPos;
                this.mShadowBuffers[4] = xPos;
                this.mShadowBuffers[6] = xPos;

                if (open > close) {
                    this.mShadowBuffers[1] = high * phaseY;
                    this.mShadowBuffers[3] = open * phaseY;
                    this.mShadowBuffers[5] = low * phaseY;
                    this.mShadowBuffers[7] = close * phaseY;
                } else if (open < close) {
                    this.mShadowBuffers[1] = high * phaseY;
                    this.mShadowBuffers[3] = close * phaseY;
                    this.mShadowBuffers[5] = low * phaseY;
                    this.mShadowBuffers[7] = open * phaseY;
                } else {
                    this.mShadowBuffers[1] = high * phaseY;
                    this.mShadowBuffers[3] = open * phaseY;
                    this.mShadowBuffers[5] = low * phaseY;
                    this.mShadowBuffers[7] = this.mShadowBuffers[3];
                }

                trans.pointValuesToPixel(this.mShadowBuffers);

                // draw the shadows

                if (dataSet.getShadowColorSameAsCandle()) {

                    if (open > close) {
                        this.mRenderPaint.setColor(
                                dataSet.getDecreasingColor() == ColorTemplate.COLOR_NONE ?
                                        dataSet.getColor(j) :
                                        dataSet.getDecreasingColor()
                        );
                    } else if (open < close) {
                        this.mRenderPaint.setColor(
                                dataSet.getIncreasingColor() == ColorTemplate.COLOR_NONE ?
                                        dataSet.getColor(j) :
                                        dataSet.getIncreasingColor()
                        );
                    } else {
                        this.mRenderPaint.setColor(
                                dataSet.getNeutralColor() == ColorTemplate.COLOR_NONE ?
                                        dataSet.getColor(j) :
                                        dataSet.getNeutralColor()
                        );
                    }
                } else {
                    this.mRenderPaint.setColor(
                            dataSet.getShadowColor() == ColorTemplate.COLOR_NONE ?
                                    dataSet.getColor(j) :
                                    dataSet.getShadowColor()
                    );
                }

                this.mRenderPaint.setStyle(Paint.Style.STROKE);

                paints.concat(this.drawLines(this.mShadowBuffers, this.mRenderPaint));

                // calculate the body
                this.mBodyBuffers[0] = xPos - 0.5 + barSpace;
                this.mBodyBuffers[1] = close * phaseY;
                this.mBodyBuffers[2] = (xPos + 0.5 - barSpace);
                this.mBodyBuffers[3] = open * phaseY;

                trans.pointValuesToPixel(this.mBodyBuffers);

                // draw body differently for increasing and decreasing entry
                if (open > close) { // decreasing

                    if (dataSet.getDecreasingColor() == ColorTemplate.COLOR_NONE) {
                        this.mRenderPaint.setColor(dataSet.getColor(j));
                    } else {
                        this.mRenderPaint.setColor(dataSet.getDecreasingColor());
                    }

                    this.mRenderPaint.setStyle(dataSet.getDecreasingPaintStyle());

                    paints.push(this.drawRect(
                                this.mBodyBuffers[0], this.mBodyBuffers[3],
                                this.mBodyBuffers[2], this.mBodyBuffers[1],
                                this.mRenderPaint));

                } else if (open < close) {

                    if (dataSet.getIncreasingColor() == ColorTemplate.COLOR_NONE) {
                        this.mRenderPaint.setColor(dataSet.getColor(j));
                    } else {
                        this.mRenderPaint.setColor(dataSet.getIncreasingColor());
                    }

                    this.mRenderPaint.setStyle(dataSet.getIncreasingPaintStyle());

                    paints.push(this.drawRect(
                                this.mBodyBuffers[0], this.mBodyBuffers[1],
                                this.mBodyBuffers[2], this.mBodyBuffers[3],
                                this.mRenderPaint));
                } else { // equal values

                    if (dataSet.getNeutralColor() == ColorTemplate.COLOR_NONE) {
                        this.mRenderPaint.setColor(dataSet.getColor(j));
                    } else {
                        this.mRenderPaint.setColor(dataSet.getNeutralColor());
                    }

                    paints.push(this.drawLine(
                                this.mBodyBuffers[0], this.mBodyBuffers[1],
                                this.mBodyBuffers[2], this.mBodyBuffers[3],
                                mRenderPaint));
                }
            } else {

                this.mRangeBuffers[0] = xPos;
                this.mRangeBuffers[1] = high * phaseY;
                this.mRangeBuffers[2] = xPos;
                this.mRangeBuffers[3] = low * phaseY;

                this.mOpenBuffers[0] = xPos - 0.5 + barSpace;
                this.mOpenBuffers[1] = open * phaseY;
                this.mOpenBuffers[2] = xPos;
                this.mOpenBuffers[3] = open * phaseY;

                this.mCloseBuffers[0] = xPos + 0.5 - barSpace;
                this.mCloseBuffers[1] = close * phaseY;
                this.mCloseBuffers[2] = xPos;
                this.mCloseBuffers[3] = close * phaseY;

                trans.pointValuesToPixel(this.mRangeBuffers);
                trans.pointValuesToPixel(this.mOpenBuffers);
                trans.pointValuesToPixel(this.mCloseBuffers);

                // draw the ranges
                let barColor: number;

                if (open > close)
                    barColor = dataSet.getDecreasingColor() == ColorTemplate.COLOR_NONE
                            ? dataSet.getColor(j)
                            : dataSet.getDecreasingColor();
                else if (open < close)
                    barColor = dataSet.getIncreasingColor() == ColorTemplate.COLOR_NONE
                            ? dataSet.getColor(j)
                            : dataSet.getIncreasingColor();
                else
                    barColor = dataSet.getNeutralColor() == ColorTemplate.COLOR_NONE
                            ? dataSet.getColor(j)
                            : dataSet.getNeutralColor();

                this.mRenderPaint.setColor(barColor);
                // 划线
                paints.push(this.drawLine(
                            this.mRangeBuffers[0], this.mRangeBuffers[1],
                            this.mRangeBuffers[2], this.mRangeBuffers[3],
                            mRenderPaint));
                paints.push(this.drawLine(
                            this.mOpenBuffers[0], this.mOpenBuffers[1],
                            this.mOpenBuffers[2], this.mOpenBuffers[3],
                            mRenderPaint));
                paints.push(this.drawLine(
                            this.mCloseBuffers[0], this.mCloseBuffers[1],
                            this.mCloseBuffers[2], this.mCloseBuffers[3],
                            mRenderPaint));
            }
        }
    }

    @Override
    public drawValues(/*Canvas c*/) : void {

        // if values are drawn
        if (isDrawingValuesAllowed(this.mChart)) {

            let dataSets: JArrayList<ICandleDataSet> = this.mChart.getCandleData().getDataSets();

            for (let i: number = 0; i < dataSets.size(); i++) {

                let dataSet: number = dataSets.get(i);

                if (!shouldDrawValues(dataSet) || dataSet.getEntryCount() < 1)
                    continue;

                // apply the text-styling defined by the DataSet
                applyValueTextStyle(dataSet);

                let trans: Transformer = this.mChart.getTransformer(dataSet.getAxisDependency());

                mXBounds.set(mChart, dataSet);

                let positions: number[] = trans.generateTransformedValuesCandle(
                        dataSet, mAnimator.getPhaseX(), mAnimator.getPhaseY(), mXBounds.min, mXBounds.max);

                let yOffset: number = Utils.convertDpToPixel(5);

                let iconsOffset: MPPointF = MPPointF.getInstance(dataSet.getIconsOffset());
                iconsOffset.x = Utils.convertDpToPixel(iconsOffset.x);
                iconsOffset.y = Utils.convertDpToPixel(iconsOffset.y);

                for (let j: number = 0; j < positions.length; j += 2) {

                    let x: number = positions[j];
                    let y: number = positions[j + 1];

                    if (!mViewPortHandler.isInBoundsRight(x))
                        break;

                    if (!mViewPortHandler.isInBoundsLeft(x) || !mViewPortHandler.isInBoundsY(y))
                        continue;

                    let entry: CandleEntry = dataSet.getEntryForIndex(j / 2 + mXBounds.min);

                    if (dataSet.isDrawValuesEnabled()) {
                        this.drawValue(
                                dataSet.getValueFormatter(),
                                entry.getHigh(),
                                entry,
                                i,
                                x,
                                y - yOffset,
                                dataSet.getValueTextColor(j / 2));
                    }

                    if (entry.getIcon() != null && dataSet.isDrawIconsEnabled()) {

                        let icon: ImagePaint = entry.getIcon();

                        let paint : Paint[] = Utils.drawImage(
                                icon.getIcon(),
                                x + iconsOffset.x,
                                y + iconsOffset.y,
                                icon.width,
                                icon.height);
                    }
                }

                MPPointF.recycleInstance(iconsOffset);
            }
        }
    }

    public drawHighlighted(indices: Highlight[]) : void {

        let candleData: CandleData = this.mChart.getCandleData();

        for (high in indices) {

            let data: ICandleDataSet = candleData.getDataSetByIndex(high.getDataSetIndex());

            if (data == null || !data.isHighlightEnabled())
                continue;

            let e: CandleEntry = data.getEntryForXValue(high.getX(), high.getY());

            if (!super.isInBoundsX(e, data))
                continue;

            let lowValue: number = e.getLow() * this.mAnimator.getPhaseY();
            let highValue: number = e.getHigh() * this.mAnimator.getPhaseY();
            let y: number = (lowValue + highValue) / 2;

            let pix: MPPointD = this.mChart.getTransformer(data.getAxisDependency()).getPixelForValues(e.getX(), y);

            high.setDraw(<number>pix.x, <number>pix.y);

            // draw the lines
            drawHighlightLines(c, <number>pix.x, <number>pix.y, data);
        }
    }

    // 同步替换
    public drawLines(shadowBuffers: number[], paint: Paint): Paint[] {
        let paints: Paint[] = [];

        if (let i = 0; i < shadowBuffers.length(); i+=4 ) {
            paints.push(drawLine(shadowBuffers[0], shadowBuffers[1], shadowBuffers[2], shadowBuffers[3], paint));
        }
        return paints;
    }

    public drawRect(l: number, t: number, r: number, b: number, paint: Paint): Paint {
        let height: number = (b - t) > 0 ? (b - t) : 0;
        let width: number = (r - l) > 0 ? (r - l) : 0;
        let rectPaint: RectPaint = new RectPaint();
        rectPaint.setStroke(paint.getColor());
        rectPaint.setStyle(paint.getStyle());
        rectPaint.setWidth(width);
        rectPaint.setHeight(height);
        return rectPaint;
    }
    public drawLine(x1: number, y1: number, x2: number, y2: number, paint: Paint): Paint {
        let linePaint : LinePaint = new LinePaint();
        linePaint.setStartPoint([x1, y1])
        linePaint.setEndPoint([x2, y2])
        return linePaint;
    }
}
