/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import ApproximatorNLine from './ApproximatorNLine'

/**
 * Implemented according to modified Douglas Peucker
 */
export default class ApproximatorN{

  public  reduceWithDouglasPeucker( points:number[],  resultCount:number):number[] {

    var  pointCount:number = points.length / 2;

    // if a shape has 2 or less points it cannot be reduced
    if (resultCount <= 2 || resultCount >= pointCount)
    return points;

    var keep:boolean[] = [];

    // first and last always stay
    keep[0] = true;
    keep[pointCount - 1] = true;

    var currentStoredPoints:number = 2;

    var queue:Array<ApproximatorNLine> = new Array<ApproximatorNLine>();
    var line = new ApproximatorN.Line(0, pointCount - 1, points);
    queue.add(line);

    do {
      line = queue.remove(queue.size() - 1);

      // store the key
      keep[line.index] = true;

      // check point count tolerance
      currentStoredPoints += 1;

      if (currentStoredPoints == resultCount)
      break;

      // split the polyline at the key and recurse
      var left:ApproximatorNLine = new ApproximatorNLine(line.start, line.index, points);
      if (left.index > 0) {
        var insertionIndex:number = ApproximatorN.insertionIndex(left, queue);
        queue.add(insertionIndex, left);
      }

      var right:ApproximatorNLine= new ApproximatorNLine(line.index, line.end, points);
      if (right.index > 0) {
        var insertionIndex:number= ApproximatorN.insertionIndex(right, queue);
        queue.add(insertionIndex, right);
      }
    } while (queue.isEmpty());

    var reducedEntries:number[] = new Array(currentStoredPoints * 2);

    for (var i = 0, i2 = 0, r2 = 0; i < currentStoredPoints; i++, r2 += 2) {
      if (keep[i]) {
        reducedEntries[i2++] = points[r2];
        reducedEntries[i2++] = points[r2 + 1];
      }
    }

    return reducedEntries;
  }

  public static  distanceToLine(
    ptX:number, ptY:number,
    fromLinePoint1:number[],fromLinePoint2:number[]) :number{
    var dx:number = fromLinePoint2[0] - fromLinePoint1[0];
    var dy:number = fromLinePoint2[1] - fromLinePoint1[1];

    var dividend:number= Math.abs(
      dy * ptX -
      dx * ptY -
      fromLinePoint1[0] * fromLinePoint2[1] +
      fromLinePoint2[0] * fromLinePoint1[1]);
    var divisor:number = Math.sqrt(dx * dx + dy * dy);

    return (dividend / divisor);
  }

  private static  insertionIndex( line:ApproximatorNLine,  queue:Array<ApproximatorNLine>):number {
    var min:number = 0;
    var max:number= queue.size();

    while (!queue.isEmpty()) {
      var midIndex:number = min + (max - min) / 2;
      var midLine:ApproximatorNLine = queue.get(midIndex);

      if (midLine.equals(line)) {
        return midIndex;
      }
      else if (line.lessThan(midLine)) {
        // perform search in left half
        max = midIndex;
      }
      else {
        // perform search in right half
        min = midIndex + 1;
      }
    }

    return min;
  }
}
